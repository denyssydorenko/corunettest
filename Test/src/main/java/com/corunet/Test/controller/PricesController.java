package com.corunet.Test.controller;

import java.text.ParseException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.corunet.Test.dto.SearcherDTO;
import com.corunet.Test.model.Prices;
import com.corunet.Test.service.ServicePrices;

@RestController
@RequestMapping("/api")
public class PricesController {
	
	@Autowired
	private ServicePrices servicePrices;
	
	@GetMapping("/prices")
	public List<Prices> getAllPrices() {
		return servicePrices.getAllPrices();
	}
	
	@GetMapping("/search")
	public ResponseEntity<List<Prices>> getAllSearched(@RequestBody SearcherDTO searcher) throws ParseException {
		if(!servicePrices.getAllSearched(searcher).isEmpty()) {
			return new ResponseEntity<List<Prices>>(servicePrices.getAllSearched(searcher), HttpStatus.OK);
		}
		else {
			return new ResponseEntity<List<Prices>>(HttpStatus.NOT_FOUND);
		}
	}

}
