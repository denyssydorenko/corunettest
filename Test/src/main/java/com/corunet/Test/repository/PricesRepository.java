package com.corunet.Test.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import com.corunet.Test.model.Prices;

@Component
public interface PricesRepository extends JpaRepository<Prices, Long> {

}
