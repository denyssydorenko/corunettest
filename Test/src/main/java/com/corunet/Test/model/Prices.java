package com.corunet.Test.model;

import javax.persistence.*;

@Entity
@Table(name = "prices")
public class Prices {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
	
	@Column(name = "BRAND_ID", nullable = false)
    private Integer brandId;
 
    @Column(name = "START_DATE", nullable = false)
    private String startDate;
    
    @Column(name = "END_DATE", nullable = false)
    private String endDate;
 
    @Column(name = "PRICES_LIST", nullable = true)
    private Integer pricesList;
 
    @Column(name = "PRODUCT_ID", nullable = false)
    private Integer productId;
    
    @Column(name = "PRIORITY", nullable = false)
    private Integer priority;
    
    @Column(name = "PRICE", nullable = false)
    private Double price;
    
    @Column(name = "CURR", nullable = false)
    private String currency;
    
    public Prices() {
    }
    
    public Prices(Integer brandId, String startDate, String endDate, Integer pricesList, Integer productId, Integer priority, Double price, String currency) {
    	this.brandId=brandId;
    	this.startDate=startDate;
    	this.endDate=endDate;
    	this.pricesList=pricesList;
    	this.productId=productId;
    	this.priority=priority;
    	this.price=price;
    	this.currency=currency;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getBrandId() {
		return brandId;
	}

	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}

	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public Integer getPricesList() {
		return pricesList;
	}

	public void setPricesList(Integer pricesList) {
		this.pricesList = pricesList;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public Integer getPriority() {
		return priority;
	}

	public void setPriority(Integer priority) {
		this.priority = priority;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	@Override
	public String toString() {
		return "Prices [id=" + id + ", brandId=" + brandId + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", priceList=" + pricesList + ", productId=" + productId + ", priority=" + priority + ", price="
				+ price + ", currency=" + currency + "]";
	}
}
