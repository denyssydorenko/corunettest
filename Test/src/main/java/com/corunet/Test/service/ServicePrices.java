package com.corunet.Test.service;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.corunet.Test.dto.SearcherDTO;
import com.corunet.Test.model.Prices;
import com.corunet.Test.repository.PricesRepository;

@Service
public class ServicePrices {
	
	@Autowired
	private PricesRepository pricesRepository;
	
	public List<Prices> getAllPrices() {
		return pricesRepository.findAll();
	}
	
	public List<Prices> getAllSearched(SearcherDTO searcher) throws ParseException {
		List<Prices> prices = pricesRepository.findAll();
		
		searcher.setDate(searcher.getDate().substring(0, 10).concat(" ").concat(searcher.getDate().substring(11)).replace(".", ":"));
		prices.forEach(o1 -> o1.setStartDate(o1.getStartDate().substring(0, 10).concat(" ").concat(o1.getStartDate().substring(11)).replace(".", ":")));
		prices.forEach(o2 -> o2.setEndDate(o2.getEndDate().substring(0, 10).concat(" ").concat(o2.getEndDate().substring(11)).replace(".", ":")));
		
		List<Prices> filtered = prices.stream()
			.filter(o1 -> o1.getProductId().equals(searcher.getProductId()))
			.filter(o2 -> o2.getBrandId().equals(searcher.getBrandId()))
			.filter(o3 -> Timestamp.valueOf(searcher.getDate()).compareTo(Timestamp.valueOf(o3.getStartDate()))>0 && Timestamp.valueOf(searcher.getDate()).compareTo(Timestamp.valueOf(o3.getEndDate()))<0)
			.sorted((o4, o5) -> Long.compare(o5.getPriority(), o4.getPriority()))
			.collect(Collectors.toList());
		
		List<Prices> result = new ArrayList<Prices>();
		
		if(!filtered.isEmpty()) {
			result.add(filtered.get(0));
		}

		return result;
	}

}
