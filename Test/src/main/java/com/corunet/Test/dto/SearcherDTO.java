package com.corunet.Test.dto;

public class SearcherDTO {
	private String date;
    private Integer productId;
    private Integer brandId;
    
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public Integer getBrandId() {
		return brandId;
	}
	public void setBrandId(Integer brandId) {
		this.brandId = brandId;
	}
	@Override
	public String toString() {
		return "SearcherDTO [date=" + date + ", productId=" + productId + ", brandId=" + brandId + "]";
	}

}
